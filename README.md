# osTicket s3 email

Pulls emails from an S3 bucket holding emails. Can be used in a stack that uses Amazon SES for sending and receiving emails.

To install place the s3email.php in a folder, and install the S3 library in a "lib" folder in the same directory. Fill in the AWS credentials and bucket, then ping the URL every time you want to check the bucket.

ENSURE THE BUCKET IS DEDICATED TO EMAILS, UNDER THE CURRENT CONFIGURATION ALL FILES WILL BE PROCESSED AND DELETED