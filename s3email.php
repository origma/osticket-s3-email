<?php
/*********************************************************************
    s3email.php

    Pulls emails from an S3 bucket

    Ben McLean <ben@origma.com.au>
    Copyright (c)  2019 Origma Pty Ltd
    https://origma.com.au

    Released under the GNU General Public License WITHOUT ANY WARRANTY.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
ini_set('memory_limit', '256M'); //The concern here is having enough mem for emails with attachments.
@chdir(dirname(__FILE__).'/'); //Change dir.

require_once(INCLUDE_DIR.'class.api.php');
require_once(INCLUDE_DIR.'api.tickets.php');
require_once(INCLUDE_DIR.'class.mailparse.php');

use Aws\S3\Exception\SignatureDoesNotMatchException;
use Aws\S3\Model\MultipartUpload\UploadBuilder;
use Aws\S3\S3Client;
use Guzzle\Http\EntityBody;
use Guzzle\Stream\PhpStreamRequestFactory;

$api_key = "";
$api_secret = "";
$bucket = "";

$credentials = array(
            'key' => $api_key,
            'secret' => $api_secret
        );

$client = S3Client::factory($credentials);

$result = $client->listObjects(['Bucket'=>$bucket]);

$objects = $result["Contents"];

$edp = new ApiEmailDataParser();
$tc = new TicketApiController();

foreach($objects as $object){
	$objectContents = $client->getObject([
		"Bucket" => $bucket,
		"Key" => $object["Key"]
	]);
	
	if(!isset($objectContents["Body"])){
		continue;
	}
	
	$tc->processEmail($edp->parse($objectContents["Body"]->__toString()));
	
	$client->deleteObject([
		"Bucket" => $bucket,
		"Key" => $object["Key"]
	]);
}
?>
